import java.util.EmptyStackException;

public class Stack {
    //define variables
    private int arr[];
    private int size;
    private int index = 0;

    public Stack(int size) {
        this.size = size;
        arr = new int[size];
    }

    public void push(int element) throws IllegalStateException {
        if (isFull()) {
            throw new IllegalStateException("Stack is full");
        }
        arr[index++] = element;
    }

    // remove one element from top of the stack
    public int pop() throws EmptyStackException {
//check if the stack not empty
        if (isEmpty()) {
            throw new EmptyStackException();
        }
        return arr[--index];
    }

    public boolean isEmpty() {
        return index == 0;
    }

    //test
    public boolean isFull() {
        return (index == size);
    }

    public int size() {
        return index;
    }
}